export class Position {

    constructor(a, b = null) {

        if (a.x) {
            this.x = a.x;
            this.y = a.y;
        } else {
            this.x = a;
            this.y = b
        }
    }
}

export class Vector extends Position {

}