import { Position } from "../utils/math";

export class Pixel {
    constructor(x, y, color) {
        this.x = x
        this.y = y
        this.color = color
    }
}

export class Layer {
    constructor(name) {

        this.name = name
        this.pixels = []
        this.active = true
    }

    static fromJSON({ name, pixels }) {

        let layer = new Layer(name);
        layer.pixels = pixels;
        return layer;
    }
}

export default class EditorData {

    constructor(data) {

        this.name = data ? data.name : null;

        if (data) {

            this.layers = data.layers.map(l => Layer.fromJSON(l))
        } else {

            this.layers = [
                new Layer('general')
            ]
        }
    }

    addLayer(name, position = 0) {

        this.layers.push(new Layer, name)
        // TODO position
    }
}