
export const draw = ({ ctx, data, config, screenSize }, clear = false) => {

    const { height, width, center, pixelSize } = config.viewPort

    const drawLayer = ({ name, pixels }) => {

        pixels.forEach(({ x, y, color }) => {

            ctx.fillStyle = color
            ctx.fillRect(x * pixelSize, y * pixelSize, pixelSize, pixelSize)
        })

    }

    ctx.translate(center.x * pixelSize, center.y * pixelSize)

    if (clear) {
        ctx.clearRect(0, 0, 10000, 1000)
    }

    data.layers.filter(layer => layer.active).forEach(layer => { drawLayer(layer) })
    ctx.translate(-center.x * pixelSize, -center.y * pixelSize)
}