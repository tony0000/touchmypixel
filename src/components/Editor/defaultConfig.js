export default {

    edition: {
        currentColors: [
            '#000000',
            '#FFFFFF'
        ]
    },

    viewPort: {

        pixelSize: 20,
        center: {
            x: 0, y: 0
        },
        width: 50,
        height: 50
    }
}