import React, { useState } from 'react'
import { ChromePicker } from 'react-color';

import { PENCIL, ERASER, SELECTION } from '../constants'

var Color = require('color');

const SaveButton = ({ actions }) => {

    const [saveIcon, setSaveIcon] = useState(false)

    return (
        <button onClick={async () => {

            await actions.save()

            setSaveIcon(true)
            setTimeout(() => {
                setSaveIcon(false)
            }, 1000);


        }}>
            {
                saveIcon ? <i className='fa fas fa-check' /> : 'Save'
            }
        </button>
    )
}

export default ({ actions, config, currentTool }) => {

    const [shadingColor, setShadingColor] = useState(localStorage.getItem('editor-currentColor') || '#aaaaaa')
    const [favoriteColors, setFavoriteColors] = useState(localStorage.getItem('editor-favorite-colors') ? JSON.parse(localStorage.getItem('editor-favorite-colors')) : [])

    return (
        <div className="EditionPanel Panel">

            <SaveButton actions={actions} />
            <button onClick={actions.reset}>Reset</button>

            <hr />

            <div className="tools">

                <button onClick={() => { actions.changeCurrentTool(PENCIL) }} className={`tool ${currentTool === PENCIL && 'is-selected'}`}><i className="fa fa-pencil-alt" /></button>
                <button onClick={() => { actions.changeCurrentTool(ERASER) }} className={`tool ${currentTool === ERASER && 'is-selected'}`}><i className="fa fa-eraser" /></button>

                <div className="details">
                    {
                        currentTool === PENCIL ? (
                            <div>
                            </div>
                        ) : currentTool === ERASER ? (
                            <div>
                            </div>
                        ) : null}
                </div>

            </div>

            <hr />

            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div style={{
                    position: 'relative',
                    width: '75',
                    height: 70
                }}>

                    <div
                        className="color-squares" style={{
                            backgroundColor: config.edition.currentColors[0],
                            zIndex: 10000,
                            position: 'absolute',
                            left: -35,
                            top: 0
                        }}
                    />
                    <div
                        className="color-squares" style={{
                            backgroundColor: config.edition.currentColors[1],
                            zIndex: 9,
                            position: 'absolute',
                            left: -15,
                            top: 15
                        }}
                    />
                </div>
            </div>

            <hr />

            <div className="shadingColors">

                {[-0.4, -0.3, -0.2, -0.1, 0, 0.1, 0.2, 0.3, 0.4].map((t, i) => {

                    const color = Color(shadingColor).lighten(t).hex();

                    return (
                        <span
                            key={i}
                            onClick={() => {

                                actions.updateCurrentColor(color)
                            }}
                            className={`
                                shadingColorsItem
                                ${config.edition.currentColors[0] === color && 'is-selected'}
                            `}
                            style={{
                                backgroundColor: color
                            }}></span>
                    )
                })}

            </div>

            <ChromePicker
                color={config.edition.currentColors[0]}
                onChangeComplete={e => {

                    localStorage.setItem('editor-currentColor', e.hex)

                    setShadingColor(e.hex)

                    actions.updateCurrentColor(e.hex)
                }} />

            <button onClick={() => {

                const newData = [
                    ...favoriteColors.filter(c => c !== shadingColor),
                    shadingColor
                ]

                localStorage.setItem('editor-favorite-colors', JSON.stringify(newData))

                setFavoriteColors(newData)
            }} >
                <i className='fa fa-heart' />
            </button>

            <div className="favoriteColor">

                {favoriteColors.map((color) => (
                    <span
                        key={color}
                        onContextMenu={e => {
                            e.preventDefault();

                            setFavoriteColors(favoriteColors.filter(c => c !== color))

                        }}
                        onClick={() => {

                            actions.updateCurrentColor(color)
                            setShadingColor(color)
                        }}
                        className='favoriteColorItem' style={{ backgroundColor: color }}></span>
                ))}

            </div>
        </div>
    );
}

