import React, { useState, useEffect, useRef } from 'react';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';
import arrayMove from 'array-move';

const LayerItem = ({ layer, actions, i, currentLayer }) => {

    const [editMode, setEditMode] = useState(false)
    const inputRef = useRef(null)
    const { name, active } = layer

    useEffect(() => {
        if (editMode) {
            inputRef.current.focus()
            inputRef.current.setSelectionRange(0, inputRef.current.value.length)
        }
    })

    return (
        <div
            className={`layer-item ${currentLayer === 'layer-' + i && 'is-selected'}`}>

            <span onClick={() => { actions.setLayerActive(i, !layer.active) }}>
                {
                    active ? (<i className="fas fa-eye" key='A'></i>) : (<i className="fas fa-eye-slash" key='B'></i>)
                }
            </span>

            {editMode ?
                <input
                    className={`layer-item-name`}
                    ref={inputRef}
                    style={{ flex: 1 }}
                    defaultValue={name}
                    onKeyDown={(e) => {

                        if (e.keyCode === 27 || e.keyCode === 13) {
                            actions.updateLayerName(e.target.value, i)

                        }
                    }}
                    onBlur={e => {
                        actions.updateLayerName(e.target.value, i)
                    }} /> :
                <div
                    onContextMenu={() => {
                        actions.deleteLayer(i)
                    }}
                    onClick={() => { actions.selectLayer(i) }}
                    style={{ flex: 1 }}
                    onDoubleClick={() => { setEditMode(true) }}
                    className={`layer-item-name`}>
                    {name}
                </div>}
        </div>
    )
}

const Layers = ({ actions, data, currentLayer }) => {
    return (

        <div className="LayersPanel Panel">

            <i className="fa fa-layer-group fa-2x" />

            <hr />

            {data.layers.map((layer, i) => (
                <LayerItem key={layer.name} layer={layer} actions={actions} i={i} currentLayer={currentLayer} />
            ))}

            <button onClick={actions.addLayer}>
                <i className="fa fa-plus" />
            </button>
        </div>
    );
}

export default Layers;