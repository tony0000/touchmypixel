import React, { useState, useEffect, useRef } from 'react'

import { Vector, Position } from '../utils/math'

import { draw } from './editor.main'
import defaultConfig from './defaultConfig'
import EditorData, { Pixel, Layer } from './editorData'

import LayersPanel from './Panels/Layers'
import EditionPanel from './Panels/Edition'

import '../../style/index.scss'
import { PENCIL, ERASER } from './constants';

let data = new EditorData(localStorage.getItem('editor-data') ? JSON.parse(localStorage.getItem('editor-data')) : null);
let inputData = {
    clickIsDown: false,
    keyPressed: {
        control: false,
        alt: false,
        shift: false
    }
}

const index = (props) => {

    const canvasRef = useRef(null);
    const [screenSize, setScreenSize] = useState(new Vector(0, 0))
    const [config, setConfig] = useState(localStorage.getItem('editor-config') ? JSON.parse(localStorage.getItem('editor-config')) : defaultConfig)
    const [layerPanelKey, setLayerPanelKey] = useState(0)
    const [editionPanelKey, setEditionPanelKey] = useState(0)
    const [currentLayer, setCurrentLayer] = useState(0)
    const [currentTool, setCurrentTool] = useState(PENCIL)

    useEffect(() => {

        window.onresize = ajusteCanvasSize
        ajusteCanvasSize()

        window.onkeydown = (e) => {

            inputData.keyPressed[e.key.toLowerCase()] = true

            if (keyBinding[e.key]) {
                e.preventDefault()
                keyBinding[e.key]()
            }
        }

        window.onkeyup = e => {

            inputData.keyPressed[e.key.toLowerCase()] = false
        }

        setTimeout(update, 0);

    }, () => {

    })

    useEffect(() => {

        localStorage.setItem('editor-config', JSON.stringify(config))
    })

    const revertEditionColors = () => {

        const config = JSON.parse(localStorage.getItem('editor-config'))

        setConfig({
            ...config,
            edition: {
                ...config.edition,
                currentColors: [

                    ...config.edition.currentColors.reverse()
                ]
            }
        })
    }

    const keyBinding = {
        z: () => {
            if (inputData.keyPressed.control) {
                console.log('back')
            }
        },

        x: revertEditionColors
    }


    const screenToGridPosition = ({ x, y }) => new Position({
        x: Math.floor(x / config.viewPort.pixelSize),
        y: Math.floor(y / config.viewPort.pixelSize)
    })

    const saveData = () => {

        localStorage.setItem('editor-data', JSON.stringify(data))
    }

    const update = () => {

        localStorage.setItem('editor-data', JSON.stringify(data))

        draw({
            ctx: getCtx(),
            data,
            config,
            screenSize
        }, true)
    }

    const ajusteCanvasSize = () => {

        setScreenSize(new Vector(
            document.documentElement.clientWidth,
            window.innerHeight
        ))
    }

    const getCtx = () => canvasRef.current.getContext('2d')

    const actions = {
        reset: () => {
            data = new EditorData()

            setLayerPanelKey(layerPanelKey + 1)
            update()
        },
        updateCurrentColor: (value) => {

            const currentColors = [
                value,
                config.edition.currentColors[1]
            ]

            setConfig({
                ...config,
                edition: {
                    ...config.edition,
                    currentColors
                }
            })
        },
        updateLayerName: (name, i) => {

            data.layers[i].name = name;
            setLayerPanelKey(layerPanelKey + 1)

            update()
        },
        addLayer: () => {
            data.layers.push(new Layer('new layer'))
            setLayerPanelKey(layerPanelKey + 1)

            update()
        },
        deleteLayer: (index) => {

            if (data.layers.length === 1) return;

            if (currentLayer === data.layers.length - 1) setCurrentLayer(currentLayer - 1)

            data.layers = data.layers.filter((l, i) => i !== index)

            setLayerPanelKey(layerPanelKey + 1)

            update()
        },
        setLayerActive: (index, active) => {
            data.layers = data.layers.map((layer, i) => (
                i === index ? Object.assign(layer, {
                    active
                }) : layer
            ))
            setLayerPanelKey(layerPanelKey + 1)

            update()
        },
        selectLayer: (i) => {
            setCurrentLayer(i)
        },
        changeCurrentTool: (tool) => {
            setCurrentTool(tool)
            setEditionPanelKey(editionPanelKey + 1)
        },
        save: () => new Promise((resolve, reject) => {

            if (!data.name) return actions.saveAs();

            data.save_date = new Date()
            localStorage.setItem(`project-${data.name}`, JSON.stringify(data))

            resolve()
        })
        ,
        saveAs: () => new Promise((resolve, reject) => {

            const name = prompt('Nom du projet ?', data.name || '');

            if (name.length === 0)
                alert('merci de rentrer un nom');
            if (!name || name.length === 0) return reject();

            data.name = name;
            data.save_date = new Date()
            localStorage.setItem(`project-${data.name}`, JSON.stringify(data))
            update()
            resolve()
        })
    }

    const applyToolFromScreenPosition = (e) => {

        const { x, y } = screenToGridPosition({
            x: e.pageX,
            y: e.pageY
        })

        if (currentTool === PENCIL) {

            data.layers[currentLayer].pixels.push(new Pixel(x, y, config.edition.currentColors[0]))
        } else if (currentTool === ERASER) {
            data.layers[currentLayer].pixels = data.layers[currentLayer].pixels.filter(p => p.x !== x || p.y !== y)
        }


        update()
    }

    const canvasEvents = {
        onMouseDown: () => { inputData.clickIsDown = true },
        onMouseUp: e => {
            applyToolFromScreenPosition(e)
            inputData.clickIsDown = false
        },
        onMouseMove: e => {
            if (!inputData.clickIsDown) return
            applyToolFromScreenPosition(e)
        }
    }

    return (
        <div className="Editor" onContextMenu={e => {
            e.preventDefault()
        }}>

            <div style={{
                position: 'absolute',
                zIndex: 99999
            }}>


            </div>

            <LayersPanel actions={actions} data={data} config={config} key={layerPanelKey} currentLayer={`layer-${currentLayer}`} />
            <EditionPanel actions={actions} config={config} currentTool={currentTool} key={`edition-${editionPanelKey}`} />

            <canvas
                className="editor-canvas"
                ref={canvasRef}
                width={screenSize.x}
                height={screenSize.y}
                {...canvasEvents}
            ></canvas>

        </div>
    );
}
export default index;